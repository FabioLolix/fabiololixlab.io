# plagueOS

## Screenshot


## Description and history

>

> Developed by

| username | password |  |
|----------|----------|--|
|  |  |  |


## License and type

>


## Packaging, sources, repositories and building

>


## Table

|                       |  |
|-----------------------|--|
| Homepage              |  |
| Homepage backup       |  |
| Based on              |  |
| Status                |  |
| Architecture          |  |
| Category              |  |
| Desktop (default)     |  |
| Desktop (available)   |  |
| Source                | <https://git.envs.net/WhichDoc/plagueOS> |
| Download              | <https://git.envs.net/WhichDoc/plagueOS_ISO> |
| Release model         |  |
| Packaging             |  |
| Package management    |  |
| Installer             |  |
| Boot                  |  |
| Init                  |  |
| Shell                 |  |
| C library             |  |
| Core utils            |  |
| Compiler              |  |
| Language              |  |
| Country               |  |
| IRC                   |  |
| Forum                 |  |
| Mailing list          |  |
| Docs                  |  |
| Bugtracker            |  |
| Translation           |  |
| Donations             |  |
| Commercial            |  |
| Price                 |  |
| Social/Contact        |  |
| Social                |  |
| Social                |  |
| ArchiveOS             |  |
| Distrowatch           |  |
| Wikipedia             |  |
| on LWN.net            |  |
| Repology              |  |
| In the timeline       |  |


## Releases

* 


## Media coverage

* 


## About this page

* This page code can be found here:
* <https://gitlab.com/FabioLolix/fabiololix.gitlab.io/-/tree/master/docs/os//>
* <https://github.com/FabioLolix/fabiololix.gitlab.io/tree/master/docs/os//>
